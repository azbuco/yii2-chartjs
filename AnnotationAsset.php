<?php

namespace azbuco\chartjs;

use yii\web\AssetBundle;

class AnnotationAsset extends AssetBundle
{
    public $sourcePath = '@bower/chartjs-plugin-annotation/';

    public $js = [
        'chartjs-plugin-annotation.js'
    ];

    public $depends = [
        'azbuco\chartjs\ChartJsAsset',
    ];
}

<?php

namespace azbuco\chartjs;

use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Json;
use yii\web\JsExpression;

class ChartJs extends Widget {

    public $options = [];
    public $clientOptions = [];
    public $data = [];
    public $type;

    public $defaultSettingsAssetClass = 'azbuco\chartjs\ChartJsDefaultSettingsAsset';

    public function init()
    {
        parent::init();
        if ($this->type === null) {
            throw new InvalidConfigException("The 'type' option is required");
        }
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
    }

    public function run()
    {
        echo Html::tag('canvas', '', $this->options);
        $this->registerClientScript();
    }

    protected function registerClientScript()
    {
        $id = $this->options['id'];
        $view = $this->getView();
        
        ChartJsAsset::register($view);
        ColorschemesAsset::register($view);
        AnnotationAsset::register($view);

        $defaultSettingsAsset = $this->defaultSettingsAssetClass;
        if ($defaultSettingsAsset !== false) {
            ChartJsDefaultSettingsAsset::register($view);
        }

        $clientOptions = $this->clientOptions;

        $config = Json::encode([
            'type' => $this->type,
            'data' => $this->data ?: new JsExpression('{}'),
            'options' => $clientOptions ?: new JsExpression('{}')
        ]);

        $js = ";var chartjs_" . Inflector::variablize($id) . " = new Chart($('#{$id}')[0].getContext('2d'),{$config}); $('#{$id}').data('chartjs', chartjs_" . Inflector::variablize($id) . ");\n";
        $view->registerJs($js);
    }
}

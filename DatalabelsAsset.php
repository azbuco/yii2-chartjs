<?php

namespace azbuco\chartjs;

use yii\web\AssetBundle;

class DatalabelsAsset extends AssetBundle
{
    public $sourcePath = '@bower/chartjs-plugin-datalabels/dist';

    public $js = [
        'chartjs-plugin-datalabels.min.js'
    ];

    public $depends = [
        'azbuco\chartjs\ChartJsAsset',
    ];
}

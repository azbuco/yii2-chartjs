<?php

namespace azbuco\chartjs;

use yii\web\AssetBundle;

class ColorschemesAsset extends AssetBundle
{
    public $sourcePath = '@bower/chartjs-plugin-colorschemes/dist';

    public $js = [
        'chartjs-plugin-colorschemes.js'
    ];

    public $depends = [
        'azbuco\chartjs\ChartJsAsset',
    ];
}

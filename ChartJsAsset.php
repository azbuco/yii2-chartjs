<?php

namespace azbuco\chartjs;

use yii\web\AssetBundle;

class ChartJsAsset extends AssetBundle
{
    public $sourcePath = '@bower/chartjs/dist';

    public $js = [
        'Chart.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}

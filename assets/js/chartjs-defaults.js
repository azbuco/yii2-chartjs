/* Chart JS global configuration */

Chart.defaults.global.elements.point.radius = 2;
Chart.defaults.global.elements.line.borderWidth = 2;
Chart.defaults.global.plugins.colorschemes.scheme = 'tableau.Tableau10';

<?php

namespace azbuco\chartjs;

use yii\web\AssetBundle;

class ChartJsDefaultSettingsAsset extends AssetBundle
{
    public $sourcePath = '@azbuco/chartjs/assets';

    public $js = [
        'js/chartjs-defaults.js'
    ];

    public $depends = [
        'azbuco\chartjs\ChartJsAsset',
    ];
}